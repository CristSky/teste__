import React, {Component} from 'react';
import logo from './logo.svg';
import {LocaleProvider, Layout, Tabs} from 'antd'
import pt_BR from 'antd/lib/locale-provider/pt_BR';
import './App.css';

import CadastroMontadora from './components/cadastro_montadora'

const {Header, Content} = Layout,
    {TabPane} = Tabs;

class App extends Component {
    render() {
        return (
            <LocaleProvider locale={pt_BR}>
                <Layout>
                    <Header className="app-header">Header</Header>

                    <Layout>
                        <Content className="app-content">
                            <Tabs
                                defaultActiveKey="1"
                                tabPosition="left"
                                // style={{ height: 220 }}
                            >
                                <TabPane tab="Tab 1" key="1">
                                    <CadastroMontadora/>
                                </TabPane>

                                <TabPane tab="Tab 2" key="2">Content of tab 2</TabPane>
                            </Tabs>


                        </Content>
                    </Layout>
                </Layout>
            </LocaleProvider>
        );
    }
}

export default App;
